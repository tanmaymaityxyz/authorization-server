package com.tanmay.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.tanmay.UserNameAlreadyExistException;

@RestControllerAdvice
public class ExceptionHandler {
	@org.springframework.web.bind.annotation.ExceptionHandler(UserNameAlreadyExistException.class)
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public String onUserNameExist(UserNameAlreadyExistException exc) {
		return exc.getMessage();
		
	}

}
