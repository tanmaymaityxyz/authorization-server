package com.tanmay.service.impl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.tanmay.UserNameAlreadyExistException;
import com.tanmay.model.UserModel;
import com.tanmay.repositories.UserModelRepository;
import com.tanmay.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private UserModelRepository userModelRepository;
	
	
	public UserServiceImpl(UserModelRepository userModelRepository) {
		super();
		this.userModelRepository = userModelRepository;
	}


	@Override
	@Transactional
	public void saveUser(UserModel user) {
		
		if(userModelRepository.findByUserName(user.getUserName()).isPresent()) {
			
			throw new UserNameAlreadyExistException("userName already exist");
		}
		userModelRepository.save(user);

	}

}
