package com.tanmay.service.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.tanmay.model.CustomUserDetails;
import com.tanmay.model.UserModel;
import com.tanmay.repositories.UserModelRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {
	private final UserModelRepository userModelRepository;
	
	

	public CustomUserDetailsService(UserModelRepository userModelRepository) {
		super();
		this.userModelRepository = userModelRepository;
	}



	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserModel user = userModelRepository.findByUserName(username).orElseThrow(() -> new UsernameNotFoundException("user not found"));
		
		return new CustomUserDetails(user);
	}

}
