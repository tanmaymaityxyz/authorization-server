package com.tanmay.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tanmay.model.UserModel;
import com.tanmay.service.UserService;

@RestController
public class UserController {
	private UserService UserService;
	
	

	public UserController(com.tanmay.service.UserService userService) {
		super();
		UserService = userService;
	}



	@PostMapping("/signup")
	public String signup(@RequestBody UserModel user) {
		UserService.saveUser(user);
		return "you have been signed up";
	}
}
