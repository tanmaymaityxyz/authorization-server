package com.tanmay;

public class UserNameAlreadyExistException extends RuntimeException {

	public UserNameAlreadyExistException(String message) {
		super(message);
	}

	
}
