package com.tanmay.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tanmay.model.UserModel;

public interface UserModelRepository extends JpaRepository<UserModel, Integer> {

	public Optional<UserModel> findByUserName(String userName);
}
